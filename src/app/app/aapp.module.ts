import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Routes, RouterModule} from "@angular/router";
import {NoneLayoutComponent} from "../common/none.layout.component";
import {AppComp} from "./component/app.comp";
import {LayoutComponent} from "../common/layout.component";
import {LayoutModule} from "../common/layout.module";

const appRoutes: Routes = [
  {
    path: "app", component: LayoutComponent,
    children: [
      { path: ""     , component: AppComp }
    ]
  }
];

@NgModule({
  declarations: [
    AppComp
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: []
})
export class AappModule { }
