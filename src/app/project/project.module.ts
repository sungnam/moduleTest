import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Routes, RouterModule} from "@angular/router";
import {NoneLayoutComponent} from "../common/none.layout.component";
import {ProjectComp} from "./component/project.comp";

const projectRoutes: Routes = [
  {
    path: "project", component: NoneLayoutComponent,
    children: [
      { path: ""     , component: ProjectComp }
    ]
  }
];

@NgModule({
  declarations: [
    ProjectComp
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(projectRoutes)
  ],
  providers: []
})
export class ProjectModule { }
