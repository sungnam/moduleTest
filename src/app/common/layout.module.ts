import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Routes, RouterModule} from "@angular/router";
import {NoneLayoutComponent} from "../common/none.layout.component";
import {LayoutComponent} from "./layout.component";


@NgModule({
  declarations: [
    NoneLayoutComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  exports:[NoneLayoutComponent, LayoutComponent],
  providers: []
})
export class LayoutModule { }
