import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Routes, RouterModule} from "@angular/router";
import {AdminComp} from "./component/admin.comp";
import {NoneLayoutComponent} from "../common/none.layout.component";

const adminRoutes: Routes = [
  {
    path: "admin", component: NoneLayoutComponent,
    children: [
      { path: ""     , component: AdminComp }
    ]
  }
];

@NgModule({
  declarations: [
    AdminComp
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(adminRoutes)
  ],
  providers: []
})
export class AdminModule { }
