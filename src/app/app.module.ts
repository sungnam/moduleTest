import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RouterModule } from "@angular/router";
import { NoneLayoutComponent } from "./common/none.layout.component";
import { AdminModule } from "./admin/admin.module";
import { AappModule } from "./app/aapp.module";
import { ProjectModule } from "./project/project.module";
import { LayoutComponent } from "./common/layout.component";


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AdminModule,
    AappModule,
    ProjectModule,
    RouterModule.forRoot([])
  ],

  declarations: [
    NoneLayoutComponent,
    LayoutComponent,
    AppComponent
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
